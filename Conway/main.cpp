#include QGuiApplication
#include QQmlApplicationEngine
#include gameoflifemodel.h

int main(int argc, char argv[])
{
    QCoreApplicationsetAttribute(QtAA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    qmlRegisterTypeGameOfLifeModel(GameOfLifeModel, 1, 0, GameOfLifeModel);

    engine.load(QUrl(QStringLiteral(qrcmain.qml)));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}